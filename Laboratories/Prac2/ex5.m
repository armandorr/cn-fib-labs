function [res] = ex5(m)
    res = 0;
    for i=0:m
        res = res + ((-1/3)^i)/(2*i+1);
    end
    res = res*sqrt(12);
    return
end