function [res] = etoXn(x,n)
    res = 0;
    i = 0;
    negX = false;
    if x < 0
        negX = true;
        x = -x;
    end
    while i < n && abs(res - exp(x)) >= 0.5*10^-12 
        val = 1;
        
        for j=1:i
            val = val * x/j;
        end
        
        res = res + val;
        i = i + 1;
    end
    if negX 
        res = 1/res;
    end
    return
end
