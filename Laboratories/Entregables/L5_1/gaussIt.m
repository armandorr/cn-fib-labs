function [x,r,delta,i] = gaussIt(A,b,X,N)
    L = tril(A,-1); U = triu(A,1); D = diag(diag(A));
    d = inv(D+L); c = d*b; B = -d*U;

    r = abs(eigs(B,1));
    if r >= 1
        error('Gauss no convergent')
    else
        xp = zeros(length(b),1);
        i = 0; delta = inf;
        while i < N
            i = i+1;
            x = c+B*xp;
            delta = norm(X-x,'inf');
            xp = x;
        end
        x = xp;
    end
end