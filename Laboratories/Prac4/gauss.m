function [x,r,rel,deltas] = gauss(A,b,n,e)
    L = tril(A,-1); U = triu(A,1); D = diag(diag(A));
    d = inv(D+L); c = d*b; B = -d*U;

    r = abs(eigs(B,1));
    if r >= 1
        error('Gauss no convergent')
    else
        x = zeros(length(b),1);
        deltas = [];
        i = 0; rel = inf;
        while i < n && rel >= e
            i = i+1;
            xp = c+B*x;
            
            % r = norm(b-A*x,'inf'); % r criteria
            deltas(i) = norm(x-xp,'inf'); % delta criteria
            rel = norm(x-xp,'inf')/norm(x,'inf'); % relative criteria
            
            x = xp;
        end
    end
end