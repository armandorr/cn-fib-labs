function [x,rj,rel,deltas] = jacobiSOR(A,b,o,n,e)
    L = tril(A,-1); U = triu(A,1); D = diag(diag(A));
    d = D^-1; cwJ = o*d*b; 
    Bsor = d*((1-o)*D-o*(L+U));
    rj = abs(eigs(Bsor,1));
%     if rj >= 1
%         error('Jacobi no convergent')
%     else
        x = [0 0 0 0]';
        deltas = [];
        i = 0; rel = inf;
        while i < n && rel >= e
            i = i+1;
            xp = cwJ+Bsor*x;
            
            % r = norm(b-A*x,'inf'); % r criteria
            deltas(i) = norm(x-xp,'inf'); % delta criteria
            rel = norm(x-xp,'inf')/norm(x,'inf'); % relative criteria
            
            x = xp;
        end
%     end
end