function [x,rj,rel,deltas] = jacobi(A,b,n,e)
    L = tril(A,-1); U = triu(A,1); D = diag(diag(A));
    d = D^-1; c = d*b; B = -d*(L+U);
    rj = abs(eigs(B,1));
    if rj >= 1
        error('Jacobi no convergent')
    else
        x = [0 0 0 0]';
        deltas = [];
        i = 0; rel = inf;
        while i < n && rel >= e
            i = i+1;
            xp = c+B*x;
            
            % r = norm(b-A*x,'inf'); % r criteria
            deltas(i) = norm(x-xp,'inf'); % delta criteria
            rel = norm(x-xp,'inf')/norm(x,'inf'); % relative criteria
            
            x = xp;
        end
    end
end