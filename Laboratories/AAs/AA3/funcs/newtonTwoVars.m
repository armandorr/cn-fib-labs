function [x,k] = newtonTwoVars(f,fp,x,tolf,tolz)
    fprintf("    k       " + ...
            "    x1              " + ...
            "    x2              " + ...
            "   f(x)           " + ...
            "x(k+1)-x(k)\n")
    s = '%5.0f %19.15f %19.15f %19.15f %19.15f\n';

    res = norm(f(x(1),x(2)),'inf');
    del = inf;
    k = 0;

    fprintf(s,[],x(1),x(2),res,[])

    while del > tolz || res >= tolf
        xp = x;

        y = linsolve(fp(x(1),x(2)),(-f(x(1),x(2))));
        x = x + y';

        k = k + 1;
        del = norm(x-xp,'inf');
        res = norm(f(x(1),x(2)),'inf');

        fprintf(s,k,x(1),x(2),res,del)
    end
end