function [xs,dels,ress,k] = iteracioSimpleTwoVars(fit1x,fit2x,F,x,n,tolf,tolz)
    xs = [];
    del = inf; dels = [];
    res = inf; ress = [];

    k = 1;
    while k < n && (del > tolz || res >= tolf)
        xp = x;
        x(1) = fit1x(x(1),x(2));
        x(2) = fit2x(x(1),x(2));
        
        del = norm(x-xp,'inf');
        res = norm(F(x(1),x(2)),'inf');
        
        xs(k,:) = x;
        dels(k) = del;
        ress(k) = res;
        k = k + 1;
    end
end