function x = newton(f,fp,x)
   z = 0;
   while abs(x-z) > eps(x)
      disp(x)
      z = x;
      x = x - f(x)/fp(x);
   end
end