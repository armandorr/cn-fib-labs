function b = bisect(f,a,b,n,tol)
    fprintf("    k       " + ...
            "    a               " + ...
            "   f(a)             " + ...
            "    b               " + ...
            "   f(b)             " + ...
            "    m               " + ...
            "   f(m)             " + ...
            "    l\n")
    s = '%5.0f %19.15f %19.15f %19.15f %19.15f %19.15f %19.15f %19.15f\n';
    del = inf;
    k = 0;
    while k < n && del > tol
        m = (a+b)/2;
        if sign(f(m)) == sign(f(b))
            b = m;
        else 
            a = m;
        end
        k = k+1;
        del = abs(b-a);
        fprintf(s,k,a,f(a),b,f(b),m,f(m),del)
    end
end