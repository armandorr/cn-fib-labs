function b = secant(f,a,b)
   s = '%5.0f %19.15f %23.15e\n';
   fprintf(s,1,a,f(a))
   fprintf(s,2,b,f(b))
   k = 2;
   while abs(b-a) > eps(abs(b))
      c = a;
      a = b;
      b = b + (b - c)/(f(c)/f(b) - 1);
      k = k+1;
      fprintf(s,k,b,f(b))
end